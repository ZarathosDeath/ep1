#ifndef ELEITOR_HPP
#define ELEITOR_HPP

#include <bits/stdc++.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>

using namespace std;

class Eleitor {
  private:
    string nome_eleitor;
    string voto_presidente;
    string voto_deputado_federal;
    string voto_deputado_distrital;
    string voto_governador;
    string voto_senador_1;
    string voto_senador_2;

  public:
    Eleitor();
    Eleitor(string nome_eleitor, string voto_deputado_federal, string voto_deputado_distrital, string voto_senador_1, string voto_senador_2, string voto_governador, string voto_presidente);
    ~Eleitor();

    string getNOME_ELEITOR();
    void setNOME_ELEITOR(string nome_eleitor);
    string getVOTO_PRESIDENTE();
    void setVOTO_PRESIDENTE(string voto_presidente);
    string getVOTO_DEPUTADO_FEDERAL();
    void setVOTO_DEPUTADO_FEDERAL(string voto_deputado_federal);
    string getVOTO_DEPUTADO_DISTRITAL();
    void setVOTO_DEPUTADO_DISTRITAL(string voto_deputado_distrital);
    string getVOTO_GOVERNADOR();
    void setVOTO_GOVERNADOR(string voto_governador);
    string getVOTO_SENADOR_1();
    void setVOTO_SENADOR_1(string voto_senador_1);
    string getVOTO_SENADOR_2();
    void setVOTO_SENADOR_2(string voto_senador_2);
};

#endif
