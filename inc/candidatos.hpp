#ifndef CANDIDATOS_HPP
#define CANDIDATOS_HPP

#include <bits/stdc++.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>

using namespace std;

class Candidatos {
  private:
    string nr_candidato;
    string nm_urna_candidato;
    string nm_partido;
    string sg_partido;
    string ds_cargo;
    string nm_ue;

  public:
    Candidatos();
    Candidatos(string nm_ue, string ds_cargo, string nr_candidato, string nm_urna_candidato, string sg_partido, string nm_partido);
    ~Candidatos();

    string getNR_CANDIDATO();
    void setNR_CANDIDATO(string nr_candidato);
    string getNM_URNA_CANDIDATO();
    void setNM_URNA_CANDIDATO(string nm_urna_candidato);
    string getNM_PARTIDO();
    void setNM_PARTIDO(string nm_partido);
    string getSG_PARTIDO();
    void setSG_PARTIDO(string sg_partido);
    string getDS_CARGO();
    void setDS_CARGO(string ds_cargo);
    string getNM_UE();
    void setNM_UE(string nm_ue);
};

#endif
