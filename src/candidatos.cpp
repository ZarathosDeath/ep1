#include "../inc/candidatos.hpp"

Candidatos::Candidatos(){
  nm_ue = ' ';
  ds_cargo = ' ';
  nr_candidato = ' ';
  nm_urna_candidato = ' ';
  sg_partido = ' ';
  nm_partido = ' ';
}

Candidatos::Candidatos(string nm_ue, string ds_cargo, string nr_candidato, string nm_urna_candidato, string sg_partido, string nm_partido){
  this->nm_ue = nm_ue;
  this->ds_cargo = ds_cargo;
  this->nr_candidato = nr_candidato;
  this->nm_urna_candidato = nm_urna_candidato;
  this->sg_partido = sg_partido;
  this->nm_partido = nm_partido;
}

Candidatos::~Candidatos(){}

string Candidatos::getNM_UE(){
  return nm_ue;
}
void Candidatos::setNM_UE(string nm_ue){
  this->nm_ue = nm_ue;
}
string Candidatos::getNR_CANDIDATO(){
  return nr_candidato;
}
void Candidatos::setNR_CANDIDATO(string nr_candidato){
  this->nr_candidato = nr_candidato;
}
string Candidatos::getSG_PARTIDO(){
  return sg_partido;
}
void Candidatos::setSG_PARTIDO(string sg_partido){
  this->sg_partido = sg_partido;
}
string Candidatos::getNM_PARTIDO(){
  return nm_partido;
}
void Candidatos::setNM_PARTIDO(string nm_partido){
  this->nm_partido = nm_partido;
}
string Candidatos::getDS_CARGO(){
  return ds_cargo;
}
void Candidatos::setDS_CARGO(string ds_cargo){
  this->ds_cargo = ds_cargo;
}
string Candidatos::getNM_URNA_CANDIDATO(){
  return nm_urna_candidato;
}
void Candidatos::setNM_URNA_CANDIDATO(string nm_urna_candidato){
  this->nm_urna_candidato = nm_urna_candidato;
}
