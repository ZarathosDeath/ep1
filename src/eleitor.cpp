#include "../inc/eleitor.hpp"

Eleitor::Eleitor(){
  nome_eleitor = " ";
  voto_deputado_federal = "0";
  voto_deputado_distrital = "0";
  voto_senador_1 = "0";
  voto_senador_2 = "0";
  voto_governador = "0";
  voto_presidente = "0";
}

Eleitor::Eleitor(string nome_eleitor, string voto_deputado_federal, string voto_deputado_distrital, string voto_senador_1, string voto_senador_2, string voto_governador, string voto_presidente){
  this->nome_eleitor = nome_eleitor;
  this->voto_deputado_federal = voto_deputado_federal;
  this->voto_deputado_distrital = voto_deputado_distrital;
  this->voto_senador_1 = voto_senador_1;
  this->voto_senador_2 = voto_senador_2;
  this->voto_governador = voto_governador;
  this->voto_presidente = voto_presidente;
}

Eleitor::~Eleitor(){}

string Eleitor::getNOME_ELEITOR(){
  return nome_eleitor;
}
void Eleitor::setNOME_ELEITOR(string nome_eleitor){
  this->nome_eleitor = nome_eleitor;
}
string Eleitor::getVOTO_PRESIDENTE(){
  return voto_presidente;
}
void Eleitor::setVOTO_PRESIDENTE(string voto_presidente){
  this->voto_presidente = voto_presidente;
}
string Eleitor::getVOTO_DEPUTADO_FEDERAL(){
  return voto_deputado_federal;
}
void Eleitor::setVOTO_DEPUTADO_FEDERAL(string voto_deputado_federal){
  this->voto_deputado_federal = voto_deputado_federal;
}
string Eleitor::getVOTO_DEPUTADO_DISTRITAL(){
  return voto_deputado_distrital;
}
void Eleitor::setVOTO_DEPUTADO_DISTRITAL(string voto_deputado_distrital){
  this->voto_deputado_distrital = voto_deputado_distrital;
}
string Eleitor::getVOTO_GOVERNADOR(){
  return voto_governador;
}
void Eleitor::setVOTO_GOVERNADOR(string voto_governador){
  this->voto_governador = voto_governador;
}
string Eleitor::getVOTO_SENADOR_1(){
  return voto_senador_1;
}
void Eleitor::setVOTO_SENADOR_1(string voto_senador_1){
  this->voto_senador_1 = voto_senador_1;
}
string Eleitor::getVOTO_SENADOR_2(){
  return voto_senador_2;
}
void Eleitor::setVOTO_SENADOR_2(string voto_senador_2){
  this->voto_senador_2 = voto_senador_2;
}
