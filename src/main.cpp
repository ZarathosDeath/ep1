/*
Aluno: Ernando da Silva Braga    
Matricula: 16/0151732     
Matéria: Orientação a Objetos
Professor: Renato Coral
*/

#include "../inc/candidatos.hpp"
#include "../inc/eleitor.hpp"

using namespace std;

void urna(vector<Candidatos>&, vector<Eleitor>&);

int main() {
    cout << "Eleições 2018" << endl;

    vector<Candidatos> lista_de_candidatos;
    vector<Eleitor> lista_de_eleitores;

    urna(lista_de_candidatos, lista_de_eleitores);

    return 0;
}

void urna(vector<Candidatos>& newlista_de_candidatos, vector<Eleitor>& newlista_de_eleitores){
  ifstream leitura_presidente("../data/consulta_cand_2018_BR.csv");

  string ue, cargo, numero, candidato, sigla, partido;

  while(leitura_presidente.good()){
      getline(leitura_presidente, ue, ',');
      getline(leitura_presidente, cargo, ',');
      getline(leitura_presidente, numero, ',');
      getline(leitura_presidente, candidato, ',');
      getline(leitura_presidente, sigla, ',');
      getline(leitura_presidente, partido);

      Candidatos newCandidatos(ue, cargo, numero, candidato, sigla, partido);
      newlista_de_candidatos.push_back(newCandidatos);
    }
  newlista_de_candidatos.pop_back();

  ifstream leitura_outros("../data/consulta_cand_2018_DF.csv");
  while(leitura_outros.good()){
      getline(leitura_outros, ue, ',');
      getline(leitura_outros, cargo, ',');
      getline(leitura_outros, numero, ',');
      getline(leitura_outros, candidato, ',');
      getline(leitura_outros, sigla, ',');
      getline(leitura_outros, partido);

      Candidatos newCandidatos(ue, cargo, numero, candidato, sigla, partido);
      newlista_de_candidatos.push_back(newCandidatos);
    }
  newlista_de_candidatos.pop_back();

  bool voto = true;
  string presidente, governador, senador_1, senador_2, deputado_federal, deputado_distrital, decisao, nome;
  int i, aux, a, deputado_federal_branco = 0, deputado_distrital_branco = 0, senador_1_branco = 0, senador_2_branco = 0, governador_branco = 0, presidente_branco = 0, nr_eleitores;
  int size_candidatos = newlista_de_candidatos.size();

  cout << "Digite o número de eleitores: ";
  cin >> nr_eleitores;
  system("clear");

  for(aux = 0; aux < nr_eleitores; aux++){
    cout << "Digite seu nome: ";
    cin >> nome;
    system("clear");

    while(voto){
      cout << "Votar para deputado federal." << endl << "Digite 0 para votar nulo." << endl << "Digite BRANCO para votar em branco." << endl << "Número do candidato: ";
      cin >> deputado_federal;
      system("clear");
      if(deputado_federal == "0"){
        cout << "Voto nulo." << endl << "Digite CONFIRMA para votar nulo." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA") break;
        else if(decisao == "CORRIGE") continue;
      }
      else if(deputado_federal == "BRANCO"){
        cout << "Voto em branco." << endl << "Digite CONFIRMA para votar em branco." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          deputado_federal_branco++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
      }
      for(i = 0; i<size_candidatos; i++){
        if(deputado_federal == newlista_de_candidatos[i].getNR_CANDIDATO()){
          cout << "Deputado federal" << endl;
          cout << "Número: " << newlista_de_candidatos[i].getNR_CANDIDATO() << endl;
          cout << "Nome: " << newlista_de_candidatos[i].getNM_URNA_CANDIDATO() << endl;
          cout << "Partido: " << newlista_de_candidatos[i].getNM_PARTIDO() << "(" << newlista_de_candidatos[i].getSG_PARTIDO() << ")" << endl;
        }
      }
      cout << "Digite CONFIRMA para votar neste candidato." << endl << "Digite CORRIGE para votar em outro candidato." << endl;
      cin >> decisao;
      system("clear");
      if(decisao == "CONFIRMA") break;
      else if(decisao == "CORRIGE") continue;
    }

    while(voto){
      cout << "Votar para deputado distrital." << endl << "Digite 0 para votar nulo." << endl << "Digite BRANCO para votar em branco." << endl << "Número do candidato: ";
      cin >> deputado_distrital;
      system("clear");
      if(deputado_distrital == "0"){
        cout << "Voto nulo." << endl << "Digite CONFIRMA para votar nulo." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA") break;
        else if(decisao == "CORRIGE") continue;
      }
      else if(deputado_distrital == "BRANCO"){
        cout << "Voto em branco." << endl << "Digite CONFIRMA para votar em branco." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          deputado_distrital_branco++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
      }
      for(i = 0; i<size_candidatos; i++){
        if(deputado_distrital == newlista_de_candidatos[i].getNR_CANDIDATO()){
          cout << "Deputado federal" << endl;
          cout << "Número: " << newlista_de_candidatos[i].getNR_CANDIDATO() << endl;
          cout << "Nome: " << newlista_de_candidatos[i].getNM_URNA_CANDIDATO() << endl;
          cout << "Partido: " << newlista_de_candidatos[i].getNM_PARTIDO() << "(" << newlista_de_candidatos[i].getSG_PARTIDO() << ")" << endl;
        }
      }
      cout << "Digite CONFIRMA para votar neste candidato." << endl << "Digite CORRIGE para votar em outro candidato." << endl;
      cin >> decisao;
      system("clear");
      if(decisao == "CONFIRMA") break;
      else if(decisao == "CORRIGE") continue;
    }

    while(voto){
      cout << "Votar para senador (1º vaga)." << endl << "Digite 0 para votar nulo." << endl << "Digite BRANCO para votar em branco." << endl << "Número do candidato: ";
      cin >> senador_1;
      system("clear");
      if(senador_1 == "0"){
        cout << "Voto nulo." << endl << "Digite CONFIRMA para votar nulo." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA") break;
        else if(decisao == "CORRIGE") continue;
      }
      else if(senador_1 == "BRANCO"){
        cout << "Voto em branco." << endl << "Digite CONFIRMA para votar em branco." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          senador_1_branco++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
      }
      for(i = 0; i<size_candidatos; i++){
        if(senador_1 == newlista_de_candidatos[i].getNR_CANDIDATO() && newlista_de_candidatos[i].getDS_CARGO() == "SENADOR"){
          cout << "Senador - 1º vaga" << endl;
          cout << "Número: " << newlista_de_candidatos[i].getNR_CANDIDATO() << endl;
          cout << "Nome: " << newlista_de_candidatos[i].getNM_URNA_CANDIDATO() << endl;
          cout << "Partido: " << newlista_de_candidatos[i].getNM_PARTIDO() << "(" << newlista_de_candidatos[i].getSG_PARTIDO() << ")" << endl;
          for(a = 0; a<size_candidatos; a++){
            if(senador_1 == newlista_de_candidatos[a].getNR_CANDIDATO() && newlista_de_candidatos[a].getDS_CARGO() == "1º SUPLENTE" && newlista_de_candidatos[a].getNM_PARTIDO() == newlista_de_candidatos[i].getNM_PARTIDO()){
              cout << "1º Suplente: " << newlista_de_candidatos[a].getNM_URNA_CANDIDATO() << endl;
            }
          }
          for(a = 0; a<size_candidatos; a++){
            if(senador_1 == newlista_de_candidatos[a].getNR_CANDIDATO() && newlista_de_candidatos[a].getDS_CARGO() == "2º SUPLENTE" && newlista_de_candidatos[a].getNM_PARTIDO() == newlista_de_candidatos[i].getNM_PARTIDO()){
              cout << "2º Suplente: " << newlista_de_candidatos[a].getNM_URNA_CANDIDATO() << endl;
            }
          }
        }
      }
      cout << "Digite CONFIRMA para votar neste candidato." << endl << "Digite CORRIGE para votar em outro candidato." << endl;
      cin >> decisao;
      system("clear");
      if(decisao == "CONFIRMA") break;
      else if(decisao == "CORRIGE") continue;
    }

    while(voto){
      cout << "Votar para senador (2º vaga)." << endl << "Digite 0 para votar nulo." << endl << "Digite BRANCO para votar em branco." << endl << "Número do candidato: ";
      cin >> senador_2;
      system("clear");
      if(senador_2 == "0"){
        cout << "Voto nulo." << endl << "Digite CONFIRMA para votar nulo." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA") break;
        else if(decisao == "CORRIGE") continue;
      }
      else if(senador_2 == "BRANCO"){
        cout << "Voto em branco." << endl << "Digite CONFIRMA para votar em branco." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          senador_2_branco++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
      }
      for(i = 0; i<size_candidatos; i++){
        if(senador_2 == newlista_de_candidatos[i].getNR_CANDIDATO() && newlista_de_candidatos[i].getDS_CARGO() == "SENADOR"){
          cout << "Senador - 2º vaga" << endl;
          cout << "Número: " << newlista_de_candidatos[i].getNR_CANDIDATO() << endl;
          cout << "Nome: " << newlista_de_candidatos[i].getNM_URNA_CANDIDATO() << endl;
          cout << "Partido: " << newlista_de_candidatos[i].getNM_PARTIDO() << "(" << newlista_de_candidatos[i].getSG_PARTIDO() << ")" << endl;
          for(a = 0; a<size_candidatos; a++){
            if(senador_2 == newlista_de_candidatos[a].getNR_CANDIDATO() && newlista_de_candidatos[a].getDS_CARGO() == "1º SUPLENTE" && newlista_de_candidatos[a].getNM_PARTIDO() == newlista_de_candidatos[i].getNM_PARTIDO()){
              cout << "1º Suplente: " << newlista_de_candidatos[a].getNM_URNA_CANDIDATO() << endl;
            }
          }
          for(a = 0; a<size_candidatos; a++){
            if(senador_2 == newlista_de_candidatos[a].getNR_CANDIDATO() && newlista_de_candidatos[a].getDS_CARGO() == "2º SUPLENTE" && newlista_de_candidatos[a].getNM_PARTIDO() == newlista_de_candidatos[i].getNM_PARTIDO()){
              cout << "2º Suplente: " << newlista_de_candidatos[a].getNM_URNA_CANDIDATO() << endl;
            }
          }
        }
      }
      cout << "Digite CONFIRMA para votar neste candidato." << endl << "Digite CORRIGE para votar em outro candidato." << endl;
      cin >> decisao;
      system("clear");
      if(decisao == "CONFIRMA") break;
      else if(decisao == "CORRIGE") continue;
    }

    while(voto){
      cout << "Votar para governador." << endl << "Digite 0 para votar nulo." << endl << "Digite BRANCO para votar em branco." << endl << "Número do candidato: ";
      cin >> governador;
      system("clear");
      if(governador == "0"){
        cout << "Voto nulo." << endl << "Digite CONFIRMA para votar nulo." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA") break;
        else if(decisao == "CORRIGE") continue;
      }
      else if(governador == "BRANCO"){
        cout << "Voto em branco." << endl << "Digite CONFIRMA para votar em branco." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          governador_branco++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
      }
      for(i = 0; i<size_candidatos; i++){
        if(governador == newlista_de_candidatos[i].getNR_CANDIDATO() && newlista_de_candidatos[i].getDS_CARGO() == "GOVERNADOR"){
          cout << "Governador" << endl;
          cout << "Número: " << newlista_de_candidatos[i].getNR_CANDIDATO() << endl;
          cout << "Nome: " << newlista_de_candidatos[i].getNM_URNA_CANDIDATO() << endl;
          cout << "Partido: " << newlista_de_candidatos[i].getNM_PARTIDO() << "(" << newlista_de_candidatos[i].getSG_PARTIDO() << ")" << endl;
          for(i = 0; i <size_candidatos; i++){
            if(governador == newlista_de_candidatos[i].getNR_CANDIDATO() && newlista_de_candidatos[i].getDS_CARGO() == "VICE-GOVERNADOR"){
              cout << "Vice-Governador: " << newlista_de_candidatos[i].getNM_URNA_CANDIDATO() << endl;
            }
          }
        }
      }
      cout << "Digite CONFIRMA para votar neste candidato." << endl << "Digite CORRIGE para votar em outro candidato." << endl;
      cin >> decisao;
      system("clear");
      if(decisao == "CONFIRMA") break;
      else if(decisao == "CORRIGE") continue;
    }

    while(voto){
      cout << "Votar para presidente." << endl << "Digite 0 para votar nulo." << endl << "Digite BRANCO para votar em branco." << endl << "Número do candidato: ";
      cin >> presidente;
      system("clear");
      if(presidente == "0"){
        cout << "Voto nulo." << endl << "Digite CONFIRMA para votar nulo." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA") break;
        else if(decisao == "CORRIGE") continue;
      }
      else if(presidente == "BRANCO"){
        cout << "Voto em branco." << endl << "Digite CONFIRMA para votar em branco." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          presidente_branco++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
      }
      for(i = 0; i<size_candidatos; i++){
        if(presidente == newlista_de_candidatos[i].getNR_CANDIDATO() && newlista_de_candidatos[i].getDS_CARGO() == "PRESIDENTE"){
          cout << "Presidente" << endl;
          cout << "Número: " << newlista_de_candidatos[i].getNR_CANDIDATO() << endl;
          cout << "Nome: " << newlista_de_candidatos[i].getNM_URNA_CANDIDATO() << endl;
          cout << "Partido: " << newlista_de_candidatos[i].getNM_PARTIDO() << "(" << newlista_de_candidatos[i].getSG_PARTIDO() << ")" << endl;
          for(i = 0; i<size_candidatos; i++){
            if(presidente == newlista_de_candidatos[i].getNR_CANDIDATO() && newlista_de_candidatos[i].getDS_CARGO() == "VICE-PRESIDENTE"){
              cout << "Vice-presidente: " << newlista_de_candidatos[i].getNM_URNA_CANDIDATO() << endl;
              break;
            }
          }
          break;
        }
      }
      cout << "Digite CONFIRMA para votar neste candidato." << endl << "Digite CORRIGE para votar em outro candidato." << endl;
      cin >> decisao;
      system("clear");
      if(decisao == "CONFIRMA") break;
      else if(decisao == "CORRIGE") continue;
    }

    Eleitor newEleitor(nome, deputado_federal, deputado_distrital, senador_1, senador_2, governador, presidente);
    newlista_de_eleitores.push_back(newEleitor);
  }

  unsigned int size = newlista_de_eleitores.size();

  for(unsigned int i = 0; i < size; i++){
    cout << "Nome: " << newlista_de_eleitores[i].getNOME_ELEITOR() << endl;
    cout << "Voto para presidente: " << newlista_de_eleitores[i].getVOTO_PRESIDENTE() << endl;
    cout << "Voto para governador: " << newlista_de_eleitores[i].getVOTO_GOVERNADOR() << endl;
    cout << "Voto para deputado federal: " << newlista_de_eleitores[i].getVOTO_DEPUTADO_FEDERAL() << endl;
    cout << "Voto para deputado distrital: " << newlista_de_eleitores[i].getVOTO_DEPUTADO_DISTRITAL() << endl;
    cout << "Voto para senador(1º vaga): " << newlista_de_eleitores[i].getVOTO_SENADOR_1() << endl;
    cout << "Voto para senador(2º vaga): " << newlista_de_eleitores[i].getVOTO_SENADOR_2() << endl;
    cout << endl;
  }
}